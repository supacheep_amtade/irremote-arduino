#include <IRremote.h>
#include <OneWire.h>
#include <DallasTemperature.h>

#include <Practicum.h>

extern "C" {
#include <usbdrv.h>
}

#define TEMP_PIN PIN_PC3
#define COUNT_TEMP 1000000

OneWire oneWire(TEMP_PIN);
DallasTemperature sensors(&oneWire);

int RECV_PIN = PIN_PC0;
IRrecv irrecv(RECV_PIN);
IRsend irsend;

unsigned int cmd25c [] = {9000, 4600, 580, 580, 580, 580, 580, 1580, 580, 580, 580, 1580, 580, 580, 580, 580, 580, 580, 580, 580, 580, 1580, 580, 580, 580, 580, 580, 1580, 580, 580, 580, 1580, 580, 580, 580, 580, 580, 580, 580, 580, 580, 580, 580, 580, 580, 580, 580, 580, 580, 580, 580, 580, 580, 580, 580, 580, 580, 580, 580, 580, 580, 580, 580, 580, 580, 580, 580, 580, 580, 580, 580, 580, 580, 580, 580, 580, 580, 580, 580, 580, 580, 580, 580, 580, 580, 1580, 600, 1580, 600, 1580, 600, 1580};

unsigned int cmdClose [] = {9000, 4400, 600, 600, 600, 600, 600, 1600, 600, 600, 600, 600, 600, 600, 600, 600, 600, 600, 600, 600, 600, 1600, 600, 600, 600, 600, 600, 600, 600, 600, 600, 1600, 600, 1600, 600, 600, 600, 600, 600, 600, 600, 600, 600, 600, 600, 600, 600, 600, 600, 600, 600, 600, 600, 600, 600, 600, 600, 600, 600, 600, 600, 600, 600, 600, 600, 600, 600, 600, 600, 600, 600, 600, 600, 600, 600, 600, 600, 600, 600, 600, 600, 600, 600, 600, 600, 1600, 600, 1600, 600, 600, 600, 1600};

unsigned int cmd18c [] = {9000, 4400, 600, 600, 600, 600, 600, 1600, 600, 600, 600, 1600, 600, 600, 600, 600, 600, 600, 600, 600, 600, 1600, 600, 600, 600, 600, 600, 600, 600, 600, 600, 1600, 600, 1600, 600, 600, 600, 600, 600, 600, 600, 600, 600, 600, 600, 600, 600, 600, 600, 600, 600, 600, 600, 600, 600, 600, 600, 600, 600, 600, 600, 600, 600, 600, 600, 600, 600, 600, 600, 600, 600, 600, 600, 600, 600, 600, 600, 600, 600, 600, 600, 600, 600, 1600, 600, 1600, 600, 1600, 600, 600, 600, 1600};

decode_results results;

// trigger
static byte trigger = 0;
static byte rec = 0;
static float counter = COUNT_TEMP;

static int sentTemp;
static float temp;
static int deviceNum;
static byte parasite;

static byte fire25c;
static byte fire18c;

void IRshoot25c()
{
  /* enforce re-enumeration, do this while interrupts are disabled! */
  usbDeviceDisconnect();
	cli();
	irsend.sendRaw(cmd25c, sizeof(cmd25c), 38);
	sei();
//	delay(1000);
	usbDeviceConnect();
}

void IRshoot18c()
{
  /* enforce re-enumeration, do this while interrupts are disabled! */
  usbDeviceDisconnect();
	cli();
	irsend.sendRaw(cmd18c, sizeof(cmd18c), 38);
	sei();
//	delay(1000);
	usbDeviceConnect();
}

void updateTemp()
{
	usbDeviceDisconnect();
	temp = sensors.getTempCByIndex(0);
	usbDeviceConnect();
}

usbMsgLen_t usbFunctionSetup(uint8_t data[8])
{
	usbRequest_t *rq = (usbRequest_t *)data;
	
	trigger = ~trigger;
	
	if (rq->bRequest == 0){
		usbMsgPtr = (uint8_t *) &sentTemp; 
		digitalWrite(PIN_PD3, trigger);
		return sizeof(usbMsgPtr);
	}
	else if (rq->bRequest == 1){
		fire25c = 1;
    digitalWrite(PIN_PD3, trigger);
		return 0;
	}
	else if (rq->bRequest == 2){
		fire18c = 1;	
    digitalWrite(PIN_PD3, trigger);
		return 0;
	}
	else if (rq->bRequest == 3){
		usbMsgPtr = (uint8_t *) &parasite; 
    digitalWrite(PIN_PD3, trigger);
		return sizeof(usbMsgPtr);
	}
	else if (rq->bRequest == 4){
		usbMsgPtr = (uint8_t *) &deviceNum; 
    digitalWrite(PIN_PD3, trigger);
		return sizeof(usbMsgPtr);
	}
#if (0)
	else if (rq->bRequest == 0){
		usbMsgPtr = (uchar *) &results.value;
    digitalWrite(PIN_PD3, trigger);
		return sizeof(usbMsgPtr);
	}
	else if (rq->bRequest == 1){
		usbMsgPtr = (uchar *) &results.decode_type;
    digitalWrite(PIN_PD3, trigger);
		return sizeof(usbMsgPtr);
	}
	else if (rq->bRequest == 2){
		usbMsgPtr = (uchar *) &results.bits;
    digitalWrite(PIN_PD3, trigger);
		return sizeof(usbMsgPtr);
	}
	else if (rq->bRequest == 3){
		usbMsgPtr = (uchar *) &results.rawlen;
    digitalWrite(PIN_PD3, trigger);
		return sizeof(usbMsgPtr);
	}
	else if (rq->bRequest == 4){
		usbMsgPtr = (uchar *) results.rawbuf;
    digitalWrite(PIN_PD3, trigger);
		return sizeof(usbMsgPtr);
	}
	else if (rq->bRequest > 4 && rq->bRequest <= results.rawlen + 4){
		usbMsgPtr = (uchar *) &results.rawbuf[rq->bRequest-5];
    digitalWrite(PIN_PD3, trigger);
		return sizeof(usbMsgPtr);
	}
#endif
	return 0;   /* default for not implemented requests: return no data back to host */
}

void setup()
{
  //pinMode(PIN_PC3, OUTPUT);
	pinMode(PIN_PD3, OUTPUT);
  fire25c = 0;
	fire18c = 0;
	deviceNum = 0;	
	irrecv.enableIRIn();  // Start the receiver
	sensors.begin();  
	
  usbInit();
  

  /* enforce re-enumeration, do this while interrupts are disabled! */
  usbDeviceDisconnect();
	deviceNum = sensors.getDeviceCount();
	parasite = sensors.isParasitePowerMode();
	sensors.requestTemperatures();
	sentTemp = (int) sensors.getTempCByIndex(0);
	delay(300);
  usbDeviceConnect();
}
 
void loop()
{
#if (0)
	if (rec)
	{
		IRshoot(cmd18c);
 		digitalWrite(PIN_PD3, trigger);
		trigger = ~trigger; 
  }
	else
	{
		if (irrecv.decode(&results) && rec == 0)
  	{
    	irrecv.resume();
    	trigger = ~trigger;
    	digitalWrite(PIN_PD3, trigger);
			rec = 1;
  	}
  }
#endif
	if (counter-- == 0)
	{
		sensors.requestTemperatures();
		sentTemp = (int) sensors.getTempCByIndex(0);
		trigger = ~trigger;
		digitalWrite(PIN_PD3, trigger);
		usbDeviceConnect();
		counter = COUNT_TEMP;
	}

	if (fire25c)
	{
		int i;
		for (i = 0; i < 3; i++)
		{
			delay(100);
			IRshoot25c();
		}
		fire25c = 0;
		usbDeviceConnect();
	}

	if (fire18c)
	{
		int i;
		for (i = 0; i < 3; i++)
		{
			delay(100);
			IRshoot18c();
		}
		fire18c = 0;
		usbDeviceConnect();
	}

	usbPoll();
}
